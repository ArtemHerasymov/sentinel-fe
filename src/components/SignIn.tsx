import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper/Paper';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox/Checkbox';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import React from 'react';
import { useStyles } from '../styles/Auth';

interface IProps {
  onSignUpPressed: () => void;
}

export const SignIn = ({ onSignUpPressed }: IProps) => {
  const classes = useStyles();
  return (
    <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            required
            fullWidth
            autoFocus
            color='primary'
            variant="outlined"
            margin="normal"
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
          />
          <TextField
            required
            fullWidth
            color='primary'
            variant="outlined"
            type="password"
            name="password"
            margin="normal"
            label="Password"
            id="password"
            autoComplete="current-password"
          />
          <FormControlLabel control={<Checkbox value="remember" color="primary" />} label="Remember me" />
          <Button fullWidth type="submit" variant="contained" color="secondary" className={classes.submit}>
            Sign In
          </Button>
          <Grid container>
            <Grid item xs>
              <Link color={'primary'} href="#" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link color={'primary'} onClick={onSignUpPressed} href="#" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Grid>
  );
};
