import React from 'react';
import { createStyles, TextField, Theme } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";

interface IProps {
  label: string;
}

export const Input = ({label}: IProps) => {
  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      container: {
        display: 'flex',
        flexWrap: 'wrap',
      },
      textField: {
        width: 500,
        borderColor: '#fff',
        marginRight: theme.spacing(1),
      },
      dense: {
        marginTop: theme.spacing(2),
      },
      menu: {
        width: 200,
      },
      inputLabel: {
        color: '#fff',
      }
    }),
  );

  const classes = useStyles();

  return (
    <TextField
      id="outlined-dense-multiline"
      label={label}
      style={{ color: '#fff' }}
      className={clsx(classes.textField, classes.dense)}
      InputProps={{
        className: classes.inputLabel,
      }}
      InputLabelProps={{
        className: classes.inputLabel,
      }}
      margin="dense"
      variant="outlined"
      multiline
      rowsMax="4"
    />
  );
};
