import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper/Paper';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField/TextField';
import React, { useState } from 'react';
import { useStyles } from '../styles/Auth';
import { Link } from '@material-ui/core';
import Button from "@material-ui/core/Button";

interface IProps {
  onSignInPressed: () => void;
  onSubmit: (email: string, companyName: string, password: string) => void;
}

export const SignUp = ({ onSignInPressed, onSubmit }: IProps) => {
  const classes = useStyles();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [companyName, setCompanyName] = useState('');
  return (
    <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            required
            fullWidth
            autoFocus
            variant="outlined"
            margin="normal"
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            onChange={(e) => setEmail(e.target.value)}
          />
          <TextField
            required
            fullWidth
            variant="outlined"
            name="companyName"
            margin="normal"
            label="Company name"
            id="companyName"
            onChange={(e) => setCompanyName(e.target.value)}
          />
          <TextField
            required
            fullWidth
            variant="outlined"
            type="password"
            name="password"
            margin="normal"
            label="Password"
            id="password"
            autoComplete="current-password"
            onChange={(e) => setPassword(e.target.value)}
          />
          <Button onClick={(e) => {
            e.preventDefault();
            onSubmit(email, password, companyName);
          }} fullWidth type="submit" variant="contained" color="secondary" className={classes.submit}>
            Sign Up
          </Button>
          <Link onClick={onSignInPressed} href="#" variant="body2">
            {'Already have an account? Sign in'}
          </Link>
        </form>
      </div>
    </Grid>
  );
};
