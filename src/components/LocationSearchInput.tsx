import React from 'react';
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';
import { SolidButton } from "./Button";

export class LocationSearchInput extends React.Component<any, {address: string}> {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(props: any) {
    super(props);
    this.state = { address: '' };
  }

  private handleChange = (address: string)=> {
    this.setState({ address });
  };

  private handleSelect = (address: string) => {
    this.setState({ address });
    geocodeByAddress(address)
      .then((results: Array<any>) => getLatLng(results[0]))
      .then((latLng: any) => console.log('Success', latLng))
      .catch((error: string)=> console.error('Error', error));
  };

  private getSuggestions = (data: {suggestions: any, loading: boolean, getSuggestionItemProps: any}) => {
    const { suggestions, loading, getSuggestionItemProps } = data;
    const suggestionsViews = suggestions.map((suggestion: any) => {
      const className = suggestion.active
        ? 'suggestion-item--active'
        : 'suggestion-item';
      const style = suggestion.active
        ? { backgroundColor: '#FF8556', cursor: 'pointer' }
        : { backgroundColor: '#ffffff', cursor: 'pointer' };
      return (
        <div
          {...getSuggestionItemProps(suggestion, {
            className,
            style: {...style, fontSize: 16, padding: 10 },
          })}
        >
          <span>{suggestion.description}</span>
        </div>
      );
    });
    return (
      <div className="autocomplete-dropdown-container">
        {loading && <div>Loading...</div>}
        {suggestionsViews}
      </div>
    )
  };

  render() {
    return (
      <div style={{ display: 'flex', flex: 1, width: 800, marginTop: 10 }}>
      <PlacesAutocomplete
        highlightFirstSuggestion={true}
        value={this.state.address}
        onChange={this.handleChange}
        onSelect={this.handleSelect}
      >
      {({ getInputProps, suggestions, getSuggestionItemProps, loading }: any) => (
      <div>
        <input
          style={{ width: 600, height: 30, fontSize: 16 }}
          value={this.state.address}
          {...getInputProps({
              placeholder: 'Search Places ...',
              className: 'location-search-input',
            })}
    />
      {this.getSuggestions({getSuggestionItemProps, loading, suggestions})}
    </div>
  )}
    </PlacesAutocomplete>
      </div>
  );
  }
}
