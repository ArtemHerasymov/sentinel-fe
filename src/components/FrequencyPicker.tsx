import React from 'react';
import Radio, { RadioProps } from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { Typography, withStyles } from "@material-ui/core";

const WhiteRadio = withStyles({
  root: {
    color: '#fff',
    '&$checked': {
      color: 'primary',
    },
  },
  checked: {},
})((props: RadioProps) => <Radio color="primary" {...props} />);

export const FrequencyPicker = () => {
  const [value, setValue] = React.useState('female');

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue((event.target as HTMLInputElement).value);
  };

  return (
    <FormControl style={{ marginTop: 20 }} component="fieldset">
      <FormLabel style={{ color: '#fff' }} component="legend">Select frequency of reviews' collection</FormLabel>
      <RadioGroup aria-label="gender" name="gender1" value={value} onChange={handleChange}>
        <FormControlLabel value="manual" control={<WhiteRadio />} label={<Typography style={{ color: '#fff' }}>Manual</Typography>}/>
        <FormControlLabel value="daily" control={<WhiteRadio />} label={<Typography style={{ color: '#fff' }}>Daily</Typography>} />
        <FormControlLabel value="weekly" control={<WhiteRadio />} label={<Typography style={{ color: '#fff' }}>Weekly</Typography>} />
        <FormControlLabel value="monthly" control={<WhiteRadio />} label={<Typography style={{ color: '#fff' }}>Monthly</Typography>} />
      </RadioGroup>
    </FormControl>
  );
};
