import React from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    button: {
      display: 'flex',
      borderRadius: 0,
      flex: 1,
      height: 57,
      marginBottom: 1,
      fontColor: '#fff',
    },
    input: {
      display: 'none',
    },
  }),
);

export interface IProps {
  label: string;
  style: any;
  onPress: () => void;
}

export const SolidButton = ({label, style, onPress}: IProps) => {
  const classes = useStyles();

  return (
      <Button onClick={onPress} style={style} color="primary" variant="outlined" className={classes.button}>
        <h1 style={{ fontSize: 16 }}>
          {label}
        </h1>
      </Button>
  );
};
