import React, { useState } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import { useStyles } from '../styles/Auth';
import { SignIn } from '../components/SignIn';
import { SignUp } from '../components/SignUp';
import { useAppInjection } from "../core/inversify";

const FLOW_TYPES = {
  SIGN_IN: 'SignIn',
  SIGN_UP: 'SignUp',
};

export const Auth = ({history} : any) => {
  const classes = useStyles();
  const App = useAppInjection();
  const [flowType, setFlowType] = useState(FLOW_TYPES.SIGN_IN);

  const getFlowContent = () => {
    if (flowType === FLOW_TYPES.SIGN_UP) {
      return <SignUp
         onSubmit={async (email, companyName, password) => {
          await App.companyController.registerCompany(email, companyName, password);
          history.push(App.repository.route);
        }}
        onSignInPressed={() => setFlowType(FLOW_TYPES.SIGN_IN)} />;
    } else {
      return <SignIn onSignUpPressed={() => setFlowType(FLOW_TYPES.SIGN_UP)} />;
    }
  };

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      {getFlowContent()}
    </Grid>
  );
};
