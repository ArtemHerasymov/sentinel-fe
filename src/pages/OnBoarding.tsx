import React, { useEffect, useState } from 'react';
import { LocationSearchInput } from "../components/LocationSearchInput";
import { Input } from "../components/Input";
import { FrequencyPicker } from "../components/FrequencyPicker";
import FormLabel from "@material-ui/core/FormLabel";
import { SolidButton } from "../components/Button";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import OnBoardingImage from '../assets/images/OnBoarding.jpg';
import Loader from 'react-loader-spinner'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: 'flex',
      flex: 1,
      height: 1000,
      flexDirection: 'column',
      paddingLeft: 50,
      paddingRight: 50,
      backgroundImage: `url(${OnBoardingImage})`,
      backgroundPosition: 'center',
      backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat',
      alignItems: 'flex-end',
      paddingTop: 100,
    },
    input: {
      display: 'none',
    },
    text: {
      color: '#fff'
    },
    locationText: {
      marginTop: 20,
      color: '#fff'
    },
    header: {
      color: '#fff',
      marginTop: 50,
    },
    loader: {
      display: 'flex',
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      alignSelf: 'center',
      marginBottom: 200,
      width: 300,
      height: 300,
    }
  }),
);

const ANIMATION_TIME = 2000;

export const OnBoarding = ({history}: any) => {
  const classes = useStyles();
  const [loading, setLoading] = useState(true);
  useEffect(() => {setTimeout(() => setLoading(false), ANIMATION_TIME)}, []);
  return (
    <div className={classes.container}>K
      {loading && <div className={classes.loader}>s
        <Loader
          type="Bars"
          color="#FF8556"
          height={150}
          width={150}
          timeout={ANIMATION_TIME}
      />
      </div>}
      {!loading && <><h1 className={classes.header}>We need some informaion about your company before we proceed</h1>
      <div style={{ marginTop: 40, display: 'flex', flexDirection: 'column', borderStyle: 'solid', borderWidth: 1, padding: 20 }}>
        <Input label={'Main administrator name'} />
        <FrequencyPicker />
        <FormLabel className={classes.locationText} component="legend">Select your company's location</FormLabel>
        <LocationSearchInput />
        <SolidButton style={{ marginTop: 20 }} label={'CONTINUE'} onPress={() => history.push('/dashboard')} />
      </div></>}
    </div>
  )
};
