import { API_SERVICE_TYPE, IApiService } from "../services/Api";
import { inject, injectable } from "inversify";
import { COMPANY_REPOSITORY_TYPE, ICompanyRepository } from "../repositories/CompanyRepository";
import { IRepository, REPOSITORY_TYPE } from "../repositories/Repository";


export interface ICompanyController {
  repository: ICompanyRepository;
  registerCompany: (email: string, password: string, companyName: string) => Promise<void>;
}

export const COMPANY_CONTROLLER_TYPE = 'UserControllerType';

@injectable()
export class CompanyController implements ICompanyController {

  public repository: ICompanyRepository;
  private appRepository: IRepository;
  private apiService: IApiService;

  constructor(
    @inject(REPOSITORY_TYPE) appRepository: IRepository,
    @inject(COMPANY_REPOSITORY_TYPE) repository: ICompanyRepository,
    @inject(API_SERVICE_TYPE) apiService: IApiService) {
    this.repository = repository;
    this.appRepository = appRepository;
    this.apiService = apiService;
  }

  public async registerCompany(email: string, password: string, companyName: string) {
    try {
      const {data, status} = await this.apiService.registerCompany(email, password, companyName);
      if (status !== 201) {
        throw new Error();
      }
      this.repository.setInitialData(data.email, data.token);
      this.appRepository.route = '/dashboard';
    } catch(e) {
      console.log(e);
    }
  }
}
