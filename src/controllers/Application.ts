import { inject, injectable } from "inversify";
import { COMPANY_CONTROLLER_TYPE, ICompanyController } from "./CompanyController";
import { IRepository, REPOSITORY_TYPE } from "../repositories/Repository";

export interface IApplication {
  repository: IRepository;
  companyController: ICompanyController;
}

export const APPLICATION_TYPE = 'ApplicationType';

@injectable()
export class Application implements  IApplication {
  public repository: IRepository;
  public companyController: ICompanyController;

  constructor(
    @inject(REPOSITORY_TYPE) repository: IRepository,
    @inject(COMPANY_CONTROLLER_TYPE) companyController: ICompanyController
  ) {
    this.repository = repository;
    this.companyController = companyController;
  }

  navigate(route: string) {
    this.repository.route = route;
  }
}
