import React, { useContext } from 'react';
import { Container, interfaces } from 'inversify';
import { Application, APPLICATION_TYPE, IApplication } from '../controllers/Application';
import { Provider as InversifyProvider } from 'inversify-react';
import { API_SERVICE_TYPE, ApiService, IApiService } from "../services/Api";
import {
  COMPANY_CONTROLLER_TYPE, CompanyController, ICompanyController
} from "../controllers/CompanyController";
import { COMPANY_REPOSITORY_TYPE, CompanyRepository, ICompanyRepository } from "../repositories/CompanyRepository";
import { IRepository, Repository, REPOSITORY_TYPE } from "../repositories/Repository";

const AppContainer = new Container({ autoBindInjectable: true });

AppContainer.bind<IApplication>(APPLICATION_TYPE)
  .to(Application)
  .inSingletonScope();
AppContainer.bind<IApiService>(API_SERVICE_TYPE)
  .to(ApiService)
  .inSingletonScope();
AppContainer.bind<ICompanyController>(COMPANY_CONTROLLER_TYPE)
  .to(CompanyController)
  .inSingletonScope();
AppContainer.bind<ICompanyRepository>(COMPANY_REPOSITORY_TYPE)
  .to(CompanyRepository)
  .inSingletonScope();
AppContainer.bind<IRepository>(REPOSITORY_TYPE)
  .to(Repository)
  .inSingletonScope();

export const appContainer = AppContainer;
export const App = AppContainer.get<IApplication>(APPLICATION_TYPE);

const InversifyContext = React.createContext<{ container: Container | null }>({ container: null });

export const Provider: React.FC<{ container: Container }> = (props: any) => {
  return (
    <InversifyContext.Provider value={{ container: props.container }}>
      <InversifyProvider container={props.container}>{props.children}</InversifyProvider>
    </InversifyContext.Provider>
  );
};

export function useInjection<T>(id: interfaces.ServiceIdentifier<T>): T {
  const { container } = useContext(InversifyContext);
  if (!container) {
    throw new Error();
  }
  return container.get<T>(id);
}

export function useAppInjection(): IApplication {
  const { container } = useContext(InversifyContext);
  const application = container!.get<IApplication>(APPLICATION_TYPE);
  if (!application) {
    throw new Error();
  }
  return application;
}
