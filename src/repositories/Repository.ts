import { injectable } from "inversify";
import { observable } from "mobx";

export interface IRepository {
  route: string;
}

export const REPOSITORY_TYPE = 'RepositoryType';

@injectable()
export class Repository implements IRepository {
  @observable route: string;

  constructor() {
    this.route = '/';
  }
}
