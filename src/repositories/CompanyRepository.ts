import { injectable } from "inversify";
import { observable } from "mobx";

export interface ICompany {
  email: string;
  token: string;
}

export interface ICompanyRepository {
  company: ICompany | null;
  setInitialData(email: string, token: string): void;
}

export const COMPANY_REPOSITORY_TYPE = 'CompanyRepositoryType';

@injectable()
export class CompanyRepository implements ICompanyRepository {

  @observable company:  ICompany | null;

  constructor(){
    this.company = null;
  }

  setInitialData(email: string, token: string) {
    this.company = { email, token };
  }
}
