import React from 'react';
import 'reflect-metadata';
import './App.css';
import { BrowserRouter as Router, Switch, withRouter } from 'react-router-dom';
import Route from './settings/Route';
import { Auth } from "./pages/Auth";
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { appContainer, Provider } from './core/inversify';
import { Dashboard } from "./pages/Dashboad";
import { OnBoarding } from "./pages/OnBoarding";

function App() {
  const innerTheme = createMuiTheme({
    palette: {
      primary: {
        main: '#FF8556',
      },
      secondary: {
        main: '#0064B4',
      }
    },
  });

  return (
    <Provider container={appContainer}>
      <ThemeProvider theme={innerTheme}>
        <Router>
          <Switch>
            <Route path="/dashboard" component={withRouter(Dashboard)} />
            <Route path="/onboarding" component={withRouter(OnBoarding)} />
            <Route exact path="/" component={withRouter(Auth)} />
          </Switch>
        </Router>
      </ThemeProvider>
    </Provider>
  );
}

export default App;
