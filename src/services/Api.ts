import { buildQuery, fetchWithTimeout } from "../utils";
import { injectable } from "inversify";

export interface IApiService {
  registerCompany(email: string, password: string, companyName: string): Promise<IResponse>;
}

const GET = 'GET';
const POST = 'POST';
const PUT = 'PUT';

export const API_SERVICE_TYPE = 'API_SERVICE_TYPE';

export interface IResponse  {
  status: number;
  statusText: string;
  data: any;
}

@injectable()
export class ApiService implements IApiService {

  private apiUrl = 'http://localhost:3000/';

  // eslint-disable-next-line no-empty-function
  constructor() {}

  public async registerCompany(email: string, password: string, companyName: string) {
    return this.request('user/register', {email, password, companyName}, POST);
  }

  private async request(
    action: string,
    params: object,
    // eslint-disable-next-line default-param-last
    method: string = POST,
    // eslint-disable-next-line default-param-last
    secure = true,
    formData?: FormData,
    headers: object = {}
  ): Promise<IResponse> {
    let url = this.apiUrl + action;
    const options: any = {
      method: method,
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin':'*',
        ...headers,
      },
    };

    if (secure) {
      // options.headers.Authorization = await this.authService.getToken();
    }

    if (formData) {
      options.body = formData;
      delete options.headers['Content-Type'];
    } else if (method === GET) {
      url = url + '?' + buildQuery(params);
    } else {
      options.body = JSON.stringify(params);
    }
    console.log(url, options);
    return fetchWithTimeout(url, options, 15000)
      .then(r => {
        console.log(r);
        return r;
      })
      .then(
        async (response): Promise<IResponse> => {
          let body = {};
          try {
            body = await response.json();
          } catch (e) {
            body = { error: 'Server response with status - ' + response.status };
          }
          const result = {
            status: response.status,
            statusText: response.statusText,
            data: body,
          };
          return result;
        }
      )
      .then(res => {
        console.log(res);
        return { ...res, data: res.data.data };
      })
      .catch(
        (error): IResponse => {
          return {
            status: 0,
            statusText: 'failed',
            data: { error: error.message },
          };
        }
      )
      .then(r => {
        console.log(r);
        return r;
      });
  }
}
