export const fetchWithTimeout = (url: string, options: any, timeout: number): Promise<any> => {
  return Promise.race([
    fetch(url, options),
    new Promise((_, reject) => setTimeout(() => reject(new Error('Timeout error')), timeout)),
  ]);
};

export const buildQuery = (params: { [key: string]: any }): string => {
  return Object.keys(params)
    .filter((key: string) => !!params[key])
    .map((key: string) => {
      if (Array.isArray(params[key])) {
        return params[key].map((item: string) => encodeURIComponent(key) + '[]=' + encodeURIComponent(item)).join('&');
      } else {
        return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
      }
    })
    .join('&');
};
