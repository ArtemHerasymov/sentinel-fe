module.exports = {
    presets: [ 'es2015', '@babel-react'],
    sourceMaps: true,
    plugins: [
        '@babel/proposal-object-rest-spread',
    ],
};
