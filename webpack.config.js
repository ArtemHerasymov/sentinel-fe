const { CheckerPlugin } = require('awesome-typescript-loader');
const HtmlWebPackPlugin = require("html-webpack-plugin");
const path = require('path');

module.exports = {

    // Currently we need to add '.ts' to the resolve.extensions array.
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx']
    },
    entry: "./src/index.tsx",
    output: {
        path: __dirname + '/dist',
        filename: 'main.js'
    },
    // Source maps support ('inline-source-map' also works)
    devtool: 'source-map',

    // Add the loader for .ts files.\
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                options: {
                    presets: ["es2015", "react"],
                    transpileOnly: true
                },
                loader: "awesome-typescript-loader"

            },
            {
                test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
                loader: 'file-loader?name=assets/[name].[hash].[ext]'
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader"
                    }
                ]
            },
            {
                test: /\.css$/,
                loader: "style-loader!css-loader"
            }
        ]
    },
    plugins: [
        new CheckerPlugin(),
        new HtmlWebPackPlugin({
            template: "./public/index.html",
            filename: "index.html",
            inject: 'body',
        })
    ],
    devServer: {
        port: 8000,
        contentBase: "./dist",
        hot: true,
        historyApiFallback: true
    },
};
